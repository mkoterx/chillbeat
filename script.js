$(document).ready(function () {

    $('#table-music-list').tablesorter();

    jQuery.ajax({
        url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PL2vrmw2gup2Jre1MK2FL72rQkzbQzFnFM&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA",
        dataType: 'jsonp',
        success: function (data) {
            var videoIDs = '';
            jQuery.each(data.items, function (index, item) {
                videoIDs += (item.snippet.resourceId.videoId + ',');
            });
            videoIDs = videoIDs.substring(0, videoIDs.length - 1);
            // console.log(videoIDs);
            jQuery.ajax({
                url: "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2Csnippet%2Cplayer&id=" +
                videoIDs + "&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA",
                dataType: 'jsonp',
                success: function (data) {
                    // console.log("https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2Csnippet%2Cplayer&id=" +
                    //     videoIDs + "&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA");
                    jQuery.each(data.items, function (index, item) {
                        var title = item.snippet.title;
                        var thumbnailUrl = item.snippet.thumbnails.default.url;
                        var $poster = $('<img>');
                        $poster.attr('src', thumbnailUrl);
                        var likeCount = item.statistics.likeCount;
                        var dislikeCount = item.statistics.dislikeCount;
                        var viewCount = item.statistics.viewCount;
                        $row = $('<tr><td>' + $poster[0].outerHTML + '</td><td>' + title + '</td><td>' + likeCount + '</td><td>' +
                            dislikeCount + '</td><td>' + viewCount + '</td></tr>');
                        var embedCode = 'https://www.youtube.com/embed/' + item.id + '?controls=0;autoplay=1;modestbranding=1&amp;fs=0&amp;rel=0&amp;hd=1&amp;disablekb=0&amp;showinfo=0&amp;iv_load_policy=0&amp;&amp;loop=0&amp';
                        $row.attr('embed-code', embedCode);
                        $row.attr('video-title', title);
                        $row.appendTo('#table-music-list');
                        $('#table-music-list').trigger('update'); // Important!!
                    });
                    $('#table-music-list').show();
                }
            });
        }
    });

    var downArrow = true;
    $('#table-music-list th').on({
        mousedown: function () {
            $(this).css('color', 'lightgrey');
        },
        mouseup: function () {
            $(this).css('color', 'white');
            if (downArrow) {
                $('small', $(this)).html("▲");
                downArrow = false;
            }
            else {
                $('small', $(this)).html("▼");
                downArrow = true;
            }
        }
    });

    function startVideoInPlayer() {
        $('iframe', $videoPlayer).attr('src', $currPlaying.attr('embed-code'));
        $('#vplayer-title').html($currPlaying.attr('video-title'));
    }

    var $videoPlayer = $('#video-player');
    var $currPlaying = null;

    var playerIsActive = false;
    $('tbody').on('click', 'tr', function () {
        $currPlaying = $(this);
        if (!playerIsActive) {
            playerIsActive = true;
            $videoPlayer.animate({
                margin: '22% 2% 2% 56%'
            }, 'fast');
        }
        startVideoInPlayer();
    });

    $('#btn-prev').on('click', function () {
        $currPlaying = $currPlaying.prev();
        startVideoInPlayer()
    })

    $('#btn-next').on('click', function () {
        $currPlaying = $currPlaying.next();
        startVideoInPlayer()
    })

    $('#btn-like').hover(function () {
        if ($(this).attr('src') != 'images/like-1.png')
            $(this).attr('src', 'images/like-hover.png');
    }, function () {
        if ($(this).attr('src') != 'images/like-1.png')
            $(this).attr('src', 'images/like-0.png');
    });

    $('#btn-like').on('click', function () {
        if ($(this).attr('src') != 'images/like-1.png') {
            $(this).attr('src', 'images/like-1.png');
            // TODO add back-end functionality for managing Favourites list
        }
        else {
            $(this).attr('src', 'images/like-0.png');
            // TODO add back-end functionality for managing Favourites list

        }
    });

    var collapsed = false;
    $('#btn-collapse').on('click', function () {
        if (!collapsed) {
            collapsed = true;
            $('#video-player').animate({
                'height': '30px',
                margin: '43.5% 2% 2% 56%'
            }, 'fast');
            $('#video-player iframe').fadeOut('fast');
            $(this).attr('src', 'images/reveal.png');
        } else {
            collapsed = false;
            $('#video-player').animate({
                'height': '43%',
                margin: '22% 2% 2% 56%',
            }, 'fast');
            $('#video-player iframe').fadeIn('fast');
            $(this).attr('src', 'images/collapse.png');
        }
    })


})
;